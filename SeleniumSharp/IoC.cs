﻿using Microsoft.Extensions.DependencyInjection;
using SeleniumSharp.Interfaces;

namespace SeleniumSharp {

    public static class IoC {

        public static IServiceCollection AddSeleniumSharp( this IServiceCollection services, string url, bool visual = true, string path = "", params string[ ] arguments ) {
            return services.AddSingleton<IChrome>( new Chrome( url, visual, path, arguments ) );
        }
    }
}