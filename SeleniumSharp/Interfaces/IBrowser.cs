﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace SeleniumSharp.Interfaces {

    public interface IBrowser : IDisposable {
        string CurrentUrl { get; }
        IWebDriver Driver { get; }
        int ProcessId { get; }
        bool IsRunning { get; }

        void Close( );

        void Refresh( );

        IWebElement Search( string xpath );

        IReadOnlyCollection<IWebElement> SearchAll( string xpath );

        void Wait( int time = 1000 );
    }
}