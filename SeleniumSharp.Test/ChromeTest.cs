﻿using System.Linq;
using Xunit;
using SeleniumSharp.Extensions;

namespace SeleniumSharp.Test {

    public class ChromeTest {
        private const string _url = @"https://www.selenium.dev/";
        private readonly Chrome _chrome = new Chrome( _url );

        [Fact]
        public void Browser_must_close( ) {
            _chrome.Close( );
        }

        [Fact]
        public void Browser_must_find_all_elements_by_xpath( ) {
            var elements = _chrome.SearchAll( "//*[contains(text(),'Selenium')]" );
            _chrome.Close( );

            Assert.True( elements.Any( ) );
        }

        [Fact]
        public void Browser_must_find_one_element_by_xpath( ) {
            var elements = _chrome.Search( "//*[contains(text(),'Selenium')]" );
            _chrome.Close( );

            Assert.True( elements != null );
        }

        [Fact]
        public void Browser_must_set_driver( ) {
            Assert.True( _chrome.Driver != null );

            _chrome.Close( );
        }

        [Fact]
        public void Browser_must_set_process_id( ) {
            Assert.True( _chrome.ProcessId > 0 );

            _chrome.Close( );
        }
    }
}