﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;

namespace SeleniumSharp.Extensions {
    public static class WebElementExtension {

        public static IWebElement ThenByXpath( this IWebElement elemem, string xpath ) =>
            elemem.ThenBy( By.XPath( xpath ) );

        public static IReadOnlyCollection<IWebElement> ThenAllByXpath( this IWebElement elem, string xpath ) =>
            elem.FindElements( By.XPath( xpath ) );

        public static IWebElement ThenByClass( this IWebElement elemem, string @class ) =>
            elemem.ThenBy( By.ClassName( @class ) );

        public static IReadOnlyCollection<IWebElement> ThenAllByClass( this IWebElement elem, string @class ) =>
            elem.FindElements( By.ClassName( @class ) );

        public static IWebElement ThenByTag( this IWebElement elemem, string tag ) =>
            elemem.ThenBy( By.TagName( tag ) );

        public static IReadOnlyCollection<IWebElement> ThenAllByTag( this IWebElement elem, string tag ) =>
            elem.FindElements( By.TagName( tag ) );

        public static IReadOnlyCollection<IWebElement> ThenAllByCss( this IWebElement elem, string css ) =>
            elem.FindElements( By.CssSelector( css ) );

        public static IWebElement ThenByCss( this IWebElement elemem, string css ) =>
            elemem.ThenBy( By.CssSelector( css ) );

        public static IWebElement ThenByName( this IWebElement elemem, string name ) =>
            elemem.ThenBy( By.Name( name ) );

        public static IReadOnlyCollection<IWebElement> ThenAllByName( this IWebElement elem, string name ) =>
            elem.FindElements( By.Name( name ) );

        private static IWebElement ThenBy( this IWebElement elemen, By by ) {
            IWebElement element = null;
            try {
                element = elemen.FindElement( by );
            } catch ( Exception ) {
                Debug.WriteLine( $"No elements found in" );
            }
            return element;
        }

        public static T GetAs<T>( this IWebElement element ) =>
            ( T ) Convert.ChangeType( element.Text, typeof( T ) );

        public static DateTime GetAsDateTime( this IWebElement element, string format ) =>
            DateTime.ParseExact( element.Text, format, CultureInfo.InvariantCulture );

        public static TimeSpan GetAsTimeSpan( this IWebElement element, string format ) =>
            TimeSpan.ParseExact( element.Text, format, CultureInfo.InvariantCulture );
    }
}
