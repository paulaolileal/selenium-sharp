﻿using SeleniumSharp.ChromeBrowser;
using System.Threading.Tasks;
using Xunit;

namespace SeleniumSharp.Test {

    public class ChromeDriverTest {

        [Fact]
        public async Task Download_must_download_driverAsync( ) {
            var version = ChromeLocal.DetectVersion( );
            await ChromeDriver.DownloadAsync( version );
        }
    }
}